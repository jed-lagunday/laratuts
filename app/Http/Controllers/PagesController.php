<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{
    public function home()
    {
        $people = ['jed', 'kosta', 'kim'];
        return view('welcome', compact('people'));
    }

    public function about()
    {
        return view('pages.about');
    }

    public function contact()
    {
        $data = array( 'Name' => 'Jed Lagunday',
            'Address' => "General Trias, Cavite",
            'Phone' => '09258891585'
        );

        return view('pages.contact', compact('data'));
    }
}
