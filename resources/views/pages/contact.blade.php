@extends('layout')

@section('subtitle')
    Contact Us
@stop

@section('content')

    <div class="title">Contact Page</div>
        @foreach($data as $title => $entry)
            <li>{{ $title }} - {{  $entry }}</li>
        @endforeach

@stop